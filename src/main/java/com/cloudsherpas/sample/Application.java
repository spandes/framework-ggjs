package com.cloudsherpas.sample;

import org.glassfish.jersey.server.ResourceConfig;
//jersey
public class Application extends ResourceConfig {
  public Application() {
    packages("com.cloudsherpas.sample.resource").
    //packages("io.swagger.sample.util").
    register(io.swagger.jaxrs.listing.ApiListingResource.class).
    register(io.swagger.jaxrs.listing.SwaggerSerializers.class);
  }
}
