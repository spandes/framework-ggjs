package com.cloudsherpas.sample.resource;

import io.swagger.annotations.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Api(value = "/hello", description = "Sample Hello World")
@Path("/hello")
public class HelloResource {

  @ApiOperation(
      value = "Sample Hello World",
      notes = "Sample Hello World"
      )
      @ApiResponses(value= {
      @ApiResponse(code = 200, message = "Successful Hello World"),
      @ApiResponse(code = 500, message = "Internal server error"),
      @ApiResponse(code = 404, message = "Not found")
      })
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String get() {
		return ("Hello World");
	}
}
