package com.cloudsherpas.sample;

import com.google.inject.Singleton;
import io.swagger.models.Contact;
import io.swagger.models.ExternalDocs;
import io.swagger.models.Info;
import io.swagger.models.License;
import io.swagger.models.Swagger;
import io.swagger.models.Tag;
import io.swagger.models.auth.ApiKeyAuthDefinition;
import io.swagger.models.auth.In;
import io.swagger.models.auth.OAuth2Definition;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

@Singleton
public class Bootstrap extends HttpServlet {
//swagger
  @Override
  public void init(ServletConfig config) throws ServletException {

    ServletContext context = config.getServletContext();
    Swagger swagger = new Swagger();
    swagger.tag(new Tag()
            .name("hello")
            .description("Sample Hello World"));
    context.setAttribute("swagger", swagger);
  }
}
