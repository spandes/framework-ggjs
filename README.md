appengine-skeleton
=============================

This is a generated application from the appengine-skeleton archetype.

Reference:
https://github.com/swagger-api/swagger-samples/tree/master/java/java-jersey2-guice

To Run:

```
mvn clean install
mvn appengine:devserver
```

To access the API
```
http://localhost:8080
```

To display "Hello World"
```
http://localhost:8080/api/hello
```